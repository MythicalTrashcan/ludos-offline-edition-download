function returnHome() {
    const button = document.querySelector('button');
    button.disabled = false;
    location.replace("../DOUBLE-CLICK-ME.html");
}

function openAbout() {
    const About = document.querySelector('about');
    window.location.href = '../about-me/index.html';
}

function openEaglerX() {
    const About = document.querySelector('eaglercraftx');
    location.replace("./EaglercraftX.html");
}

function openEagler() {
    const About = document.querySelector('eaglercraft');
    location.replace("./Eaglercraft.html");
}